function kill_data_acquisition()
    %KILL_DATA_ACQUISITION Stop an ongoing data acquisition
    global monitor
    monitor.stop()
end

