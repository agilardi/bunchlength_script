function deflector_move(status,config)
    %DEFLECTOR_MOVE 
    % Control the timing of the deflector 
    % Input parameters:
    %   status      : It is the wanted status IN (true) OUT (false)
    %   config      : Configuration struct from main program
    %    It takes the:
    %    config.deflectorIN   --> Timing to have the RF IN (value of CX.RFP30) 
    %    config.deflectorOUT  --> Timing to have the RF OUT (value of CX.RFP30) 
    %    config.deflectorSTEP --> Holding the step size for moving the deflector
    % Optional argument:
    %    config.deflectorWIDTH --> It is the length of the RF pulse
    %    config.deflectorKLYSTRON --> It is the length between klystron 
    %                                 start and RF start
   
    startPoint = matlabJapc.staticGetSignal('','CX.SRFP30/Delay#delay');
    stopPoint  = matlabJapc.staticGetSignal('','CX.ERFP30/Delay#delay');
    KlystronStartPoint  = matlabJapc.staticGetSignal('','CX.SKLY30/Delay#delay');
    
    assert (startPoint ~= stopPoint)

    if ~exist('config.deflectorWIDTH','var') || isnan(config.deflectorWIDTH)
       config.deflectorWIDTH = stopPoint - startPoint;
       assert (config.deflectorWIDTH>0)
    else
       %TODO: Implement change of width
       assert((stopPoint - startPoint) == config.deflectorWIDTH)
    end 

    if ~exist('config.deflectorKLYSTRON','var') || isnan(config.deflectorKLYSTRON)
       KLYSTRON = startPoint-KlystronStartPoint;
       assert (KLYSTRON>0)
    end
    
    if ~~(status) %Move IN
       if (startPoint == config.deflectorIN)
           disp('Already IN')
       else
           config.deflectorKLYSTRON = config.deflectorIN - KLYSTRON;
           Move(startPoint,stopPoint,config.deflectorIN,config.deflectorIN+config.deflectorWIDTH,KlystronStartPoint,-config.deflectorSTEP)
        end
    else %Move OUT
       if (startPoint == config.deflectorOUT)
           disp('Already OUT')
       else     
           config.deflectorKLYSTRON = config.deflectorOUT - KLYSTRON;
           Move(startPoint,stopPoint,config.deflectorOUT,config.deflectorOUT+config.deflectorWIDTH,KlystronStartPoint,config.deflectorSTEP)
       end
    end
end

function Move(startPoint,stopPoint,goalStartPoint,goalStopPoint,klystronStart,step)

    assert(rem(abs(goalStartPoint-startPoint),abs(step)) == 0) %Not implemented
    assert(rem(abs(goalStopPoint-stopPoint),abs(step)) == 0) %Not implemented
    
    while (startPoint~= goalStartPoint)
        startPoint = startPoint +step;  
        klystronStart = klystronStart +step;
        matlabJapc.staticSetSignal('','CX.SRFP30/Delay#delay',startPoint);
        matlabJapc.staticSetSignal('','CX.SKLY30/Delay#delay',klystronStart);
        
        stopPoint = stopPoint +step;
        matlabJapc.staticSetSignal('','CX.ERFP30/Delay#delay',stopPoint);  
        pause(0.2)
    end       
end