function beamOnOff(onOff, method)
    %BEAMONOFF Turn the beam on (true) or off (false)
    % Optional input: Method
    % Methods:
    % 1: Use laser shutters
    % 2: Use scree 0215
    % Default: 2
    
    if ~exist('method','var')
        method = 2;
    end
    
    if method == 1
        error('not implemented')
    elseif method == 2
        if ~onOff
            disp('Beam OFF')
            matlabJapc.staticSetSignal('','CA.BTV0215/Setting#screenSelect',int32(1))
        else
            disp('Beam ON')
            matlabJapc.staticSetSignal('','CA.BTV0215/Setting#screenSelect',int32(0))
        end
    else
        error('Unknown method')
    end
    
end

