function [dirname,Files] = findFilesFromID(datasetID, config)
    %FINDDATASETFOLDERFROMID Finds the path of a dataset folder given the ID
    
    if ~exist('config','var')
        config = BunchLength_config();
    end
    Files = NaN;

    %Generate expected foldername from datasetID
    datasetExpectedFolder = sprintf('dataset_%06i',datasetID);

    %Find date-specifig data folder
    dateFolder = dir(config.dataFolder);

    assert (config.dataFolder(end) == '/')
    if isempty(dateFolder)
        disp(['Could not find folder "' config.dataFolder '"'])
        return
    end
    
    j = 0; %Used for sanity check -- should only be a single dataset with same idx!
    for i = 1:length(dateFolder)
        if length(dateFolder(i).name) ~= 10
            %Not on format yyyy-mm-dd
            continue
        end
        
        subFolder = dir(strcat(config.dataFolder,dateFolder(i).name));
        if isempty(subFolder)
            continue
        end
        % Filter the list of files
        for z = 1:length(subFolder)
            if length(subFolder(z).name) < 14
                %Not on format dataset_xxxxxx(whatever)
                continue
            end
            if strcmp(subFolder(z).name(1:14), datasetExpectedFolder)
                %set return values
                dirname = strcat(config.dataFolder,dateFolder(i).name,'/',subFolder(z).name);
                Files = dir(dirname);
                j = j+1;
            end
        end
    end
        
    if (j>1)
        error('More of 1 dataset with same ID')
    elseif (j<1)
        error('No dataset found')
    end
end
