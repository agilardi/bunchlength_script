function [dataPath] = getDataPath()
    %GETDATAPATH Returns the path to the folder containing the data
    %   on this system.
    %   Kept in a separate file to simplify version control.
    
    dataPath = '/clear/data/MatLab/Experiments/BunchLength/data/';
end

