function [config] = BunchLength_config()
    %BunchLength_config : Set the default parameters
    
    config = struct();
    
    %Path to the data
    config.dataFolder = getDataPath();
    
    %RF deflector parameters
    config.deflectorIN          = 30135;
    config.deflectorOUT         = 30150;
    config.deflectorSTEP        =     1;
    config.deflectorWIDTH       =   NaN; %In order to have the value in the machine
    config.deflectorKLYSTRON    =   NaN; %In order to have the value in the machine
    
   
    %Camera parameters
    config.camName = 'CA.BTV0390';
    config.screnID = 2;
    
    config.camResX = 0.4; %pix/mm
    config.camResY = 0.4; %pix/mm
    
    %Scope parameters
    config.RFpower_scope = 'CA.SCOPE06.CH03';
    config.RFphase_scope = 'CA.SCOPE08.CH04';
    
    %Other parameters
    config.beamOnOff_method = 2;
    config.japcSelector = 'SCT.USER.SETUP';
    
end

