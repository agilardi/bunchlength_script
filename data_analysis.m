function [bunchLength, Kcal] = data_analysis(datasetID)
    %DATA_ANALYSIS Template data analysis script
    % Input:
    % - datasetID: Numerical ID of the dataset to load (int)
    %
    % Output:
    % - bunchLength
    %
    
    addpath('utils')
    %Call findDatasetfolderFromID to find the right folder
    
    %Load metadata -- comes with config!!
    
    %Process background
    
    %Process zeropos
    
    
    %Load Fist data
    
    %Check ZeroPos == First data pos
    
    %Process data
    % (loop over datafiles -- use the file listing and compare with phases
    % listed in config!!!)
    
    
    
    bunchLength = NaN;
    Kcal = NaN;
end

