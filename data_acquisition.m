function [dataset_id,dataset_path] = data_acquisition(phaseRange, Nshots)
    %DATA_ACQUISITION Acquire data for the deflector
    %
    % Input arguments:
    % - phaseRange : Array of phases to use
    %
    % Optional positional input arguments:
    % - Nshots     : Number of images per phase (and bg and reference)
    %                Default: 5
    
    try
        matlabJapc.staticINCAify('CTF')
    catch e
        fprint ('Already INCAified')
    end
    
    
    %% Load configration
    if ~exist('phaseRange', 'var')
        error('Must specify phaseRange')
    end
    if ~exist('Nshots', 'var')
        Nshots = 10;
    end
    
    addpath('utils')
    config = BunchLength_config();
    
    %Determine the next run number
    if ~exist(config.dataFolder, 'dir')
        error(['Folder "', config.dataFolder, '" does not exist!'])
    end
    dataIndexFile = [config.dataFolder, '/', 'dataIndex.mat'];
    if ~exist(dataIndexFile, 'file')
        error(['Did not find "dataIndex.mat" in "', config.dataFolder,'"'])
    end
    load(dataIndexFile, 'dataIndex');
    dataIndex = dataIndex+1;
    save(dataIndexFile, 'dataIndex','-v7.3');
    
    %Find or build folder
    todayFolder = datestr(now(), 'yyyy-mm-dd');
    todayLongFolder = [config.dataFolder, '/',todayFolder];
    if ~exist(todayLongFolder, 'dir')
        disp(['creating new folder "', todayLongFolder, '"'])
        [success,message] = mkdir(todayLongFolder);
        if success ~= 1
            error(['Failure in creating day folder, message = "', message, '"'])
        end
    end
    
    datasetFolder = sprintf('dataset_%06i',dataIndex);
    datasetLongFolder = [todayLongFolder, '/', datasetFolder];
    if exist(datasetLongFolder, 'dir')
        error(['Folder "', datasetLongFolder, '" already exist?!?'])
    end
    [success,message] = mkdir(datasetLongFolder);
    if success ~= 1
        error(['Failure in creating datset folder, message = "', message, '"'])
    end
    
    %% Get initial settings
    cameraSettings = matlabJapc.staticGetSignal('', [config.camName, '/', 'SettingAcquisition']);
    startingPhase = matlabJapc.staticGetSignal('','CK.LL-MKS30/Setting#PhaseSh_SP');
    
    %% Define parameters to monitor and setup monitor
    monitorParamList = { ...
        [config.camName, '/Acquisition'], ...
        [config.RFpower_scope, '/Acquisition'], ...
        [config.RFphase_scope, '/Acquisition'] ...
        };
    clear global dataAcqBuffer
    clear global dataAcqState
    global dataAcqBuffer;
    dataAcqBuffer = {};
    global dataAcqState
    dataAcqState = false; %true = taking data, false : paused
    global monitor;
    monitor = matlabJapcMonitor(config.japcSelector, monitorParamList, @(data,monitor)DAQ_callback(data, monitor, Nshots));
    monitor.useFastStrategy(1);
    monitor.allowManyUpdatesPerCycle(1);
    monitor.synchronizedExternalCalls = true;
    
    
    %% Save metadata to datasetFolder
    metaDataFilename = 'metadata.mat';
    metaDataLongFilename = [datasetLongFolder, '/', metaDataFilename];
    
    metadata = struct();
    metadata.config = config;
    metadata.phaseRange = phaseRange;
    metadata.monitorParamList = monitorParamList;
    metadata.cameraSettings = cameraSettings;
    metadata.startingPhase = startingPhase;
    
    save(metaDataLongFilename, 'metadata','-v7.3');
    
    disp('Inserting the selected screen')
    matlabJapc.staticSetSignal('',[config.camName,'/Setting#screenSelect'],int32(config.screnID))
    pause(10)
    %% Get background
    beamOnOff(false,config.beamOnOff_method); 

    disp('Getting background!')
    backgroundDataFilename = 'background.mat';
    backgroundDataLongFilename = [datasetLongFolder, '/', backgroundDataFilename];

    pause(1) %Make sure everything is ready...
    dataAcqState = true;
    monitor.start() %only start first time, next time use pauseOn()/pauseOff()
    while(dataAcqState)
        pause(1) %wait for acq to finish
    end
    save(backgroundDataLongFilename, 'dataAcqBuffer','-v7.3');
    dataAcqBuffer = {};
    disp('BG Done!')
    
    %% Get zeropos
    beamOnOff(true,config.beamOnOff_method); % ENABLE FOR RUNNING

    %Extract deflector
    disp('Extracting...')
    deflector_move(false,config)
        
    disp('Getting zeropos!')
    zeroposDataFilename = 'zeropos.mat';
    zeroposDataLongFilename = [datasetLongFolder, '/', zeroposDataFilename];

    pause(1) %Make sure everything is ready...
    dataAcqState = true;
    monitor.pauseOff()
    while(dataAcqState)
        pause(1) %wait for acq to finish
    end
    save(zeroposDataLongFilename, 'dataAcqBuffer','-v7.3');
    dataAcqBuffer = {};
    disp('ZeroPos done!')
    
    %% Scan phases and get data
    
    % Insert deflector
    disp('Inserting...')
    deflector_move(true,config)
    
    fprintf('Collecting data; length(phaseRange) = %i, Nshots = %i\n', ...
        length(phaseRange), Nshots);
    
    i = 1
    for ph = phaseRange
        fprintf('Phase = %f [deg],  = %i\n', ph, i);
        
        %Move deflector phase
        matlabJapc.staticSetSignal('','CK.LL-MKS30/Setting#PhaseSh_SP',ph);
        
        phaseDataFilename = sprintf('scandata_%03i.mat', i);
        phaseDataLongFilename = [datasetLongFolder, '/', phaseDataFilename];

        pause(1) %Make sure everything is ready...
        dataAcqState = true;
        monitor.pauseOff()
        while(dataAcqState)
            pause(1) %wait for acq to finish
        end
        save(phaseDataLongFilename, 'dataAcqBuffer','-v7.3');
        dataAcqBuffer = {};
        
        i = i+1;
    end
    
    %Extract deflector
    disp('Extracting...')
    deflector_move(false,config)
    
    disp('Done!')
    
    %% Cleanup
    matlabJapc.staticSetSignal('','CK.LL-MKS30/Setting#PhaseSh_SP',metadata.startingPhase);
    monitor.stop();
    clear global dataAcqBuffer;
    clear global dataAcqState;
    clear global monitor;
end

function DAQ_callback(data, monitor, Nshots)
    %Callback function from JAPC
    % Inputs:
    % - data    : The data
    % - monitor : The matlabJapcMonitor object (for control)
    % - config  : config struct; read-only please
    % - Nshots  : How many shots to record before we stop
    % - buffer  : The buffer to use during acquisition,
    %             created in the main script
    
    global dataAcqBuffer
    global dataAcqState
    assert(dataAcqState)
    
    %disp(data)
    
    dataAcqBuffer{length(dataAcqBuffer)+1} = data;
    
    %disp(dataAcqBuffer)
    
    if length(dataAcqBuffer) >= Nshots
        monitor.pauseOn()
        dataAcqState = false;
    end
end